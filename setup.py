import setuptools

setuptools.setup(
    name="StrainMAPPER",
    version="0.1",
    description="Python Package for Strain Mapping using 4D STEM and the EWPC",
    url="",
    author="Maarten Bolhuis",
    install_requires=[
        "pixstem",
        "PyQt5",
        "matplotlib<3.8.0",
        "hyperspy==1.7.6",
    ],
    author_email="m.bolhuis@tudelft.nl",
    packages=setuptools.find_packages(),
    zip_safe=False,
)

