# © StrainMAPPER developer team 2023
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import dask.array as da
from dask.diagnostics import ProgressBar
from scipy.linalg import polar
from sklearn.cluster import KMeans
from hyperspy.signals import Signal2D
from hyperspy._signals.lazy import LazySignal
import StrainMAPPER.EWPC_tools as et
from StrainMAPPER.cmap import custom_cmap
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize, CenteredNorm
import matplotlib.patches as patches
import logging
_logger = logging.getLogger(__name__)


class EWPCSignal2D(Signal2D):
    def peak_find(self, n_clusters, lazy_result = False, show_progressbar = False, **kwargs):
        """
        Finds the peaks in a dataset using scikit-image Blob Detection algorithm.
        For more information, refer to the scikit-image website:
        https://scikit-image.org/docs/stable/auto_examples/features_detection/plot_blob.html

        Parameters
        ----------
        n_clusters : int
            The number of clusters to use in the algorithm.
        lazy_result : bool, optional
            If True, the result will be a dask array, otherwise it will be computed immediately. Default is False.
        show_progressbar : bool, optional
            If True, a progress bar will be shown while computing the result. Default is False.
        **kwargs : dict, optional
            Additional keyword arguments to pass to the skimage Blob Detection.

        Returns
        -------
        peak_data : dask array or numpy array
            The peak data as a dask array or numpy array, depending on the value of lazy_result.
        """

        chunk_calculations = [16,16]+list(self.data.shape[-2:])
        if self._lazy:
            dask_array = self.data.rechunk(chunk_calculations)
        else:
            dask_array = da.from_array(self.data, chunks=chunk_calculations)
        peak_array = da.map_blocks(et.peak_find, dask_array, n_clusters,
                                    drop_axis=(2,3), dtype=object, **kwargs)

        signal_shape = self.data.shape
        if not lazy_result:
            if show_progressbar:
                pbar = ProgressBar()
                pbar.register()
            peak_array = peak_array.compute()
            peak_array = EWPCPeakArray(peak_array, signal_shape)
            if show_progressbar:
                pbar.unregister()
        else:
            peak_array = LazyEWPCPeakArray(peak_array, signal_shape)
        return peak_array

    def subpixel_peak_find(self, peak_array, kmeans, peak_cluster1, peak_cluster2, mask_r = 3,
                      threshold = 3, center = False, lazy_result = False, show_progressbar = False):
        """
        Find the subpixel maximum of the EWPC peaks using the center of mass of a masked data set.

        Parameters
        ----------
        peak_array : array_like
            The peak data, as a numpy array or dask array.
        kmeans : object
            The kmeans object used for clustering.
        peak_cluster1 : int
            The first peak cluster to use in the calculation.
        peak_cluster2 : int
            The second peak cluster to use in the calculation.
        mask_r : int, optional
            The radius of the circular mask to use in the calculation. Default is 3.
        threshold : int, optional
            The threshold to use for masking. Values less than or equal to this threshold will be masked. Default is 3.
        center : bool, optional
            If True, the center value will be subtracted from the result. Default is False.
        lazy_result : bool, optional
            If True, the result will be a dask array, otherwise it will be computed immediately. Default is False.
        show_progressbar : bool, optional
            If True, a progress bar will be shown while computing the result. Default is False.

        Returns
        -------
        EWPCPeakSignal2D or LazyEWPCPeakSignal2D
            The center of mass data as a dask array or numpy array, depending on the value of lazy_result.

        """
        
        nav_dim = self.data.shape[:2]
        sig_dim = self.data.shape[-2:]
        peak_array = peak_array.data
       
        if type(peak_array) == da.core.Array:
            _logger.warning('Peak array has the type [dask.array.core.Array] and needs to be converted to a numpy array first', RuntimeWarning)
            if show_progressbar:
                pbar = ProgressBar()
                pbar.register()
                peak_array = np.array(peak_array)
                pbar.unregister()
        
        com_list = {}
        if show_progressbar:
            print('Generating 4D mask for COM-calculations')
        for i, pc in enumerate((peak_cluster1, peak_cluster2)):
            select_peaks = et.select_cluster(peak_array, kmeans, peak_cluster= pc)
            select_peaks = select_peaks.tolist()
            select_peaks = np.array(select_peaks)
            select_peaks = np.squeeze(select_peaks)
        
            centerY = select_peaks[:,:,0]
            centerX = select_peaks[:,:,1]
            com_mask = et.make_circular_4D_mask(centerX, centerY, nav_dim+sig_dim, radius=mask_r)
            com_mask = da.invert(com_mask)
            
            com_list[i] = et._center_of_mass_array(self.data,mask_array=com_mask)
            
        com = da.stack(list(com_list.values()), axis = 1)
        com = da.ma.masked_less_equal(com, threshold)

        if center:
            com -= 64
        
        if lazy_result:
            com = LazyEWPCPeakSignal2D(com, peak_cluster1, peak_cluster2)
        else:
            if show_progressbar:
                pbar = ProgressBar()
                pbar.register()
                print('Calculating COM')
            com = com.compute()
            if show_progressbar:
                pbar.unregister()
            com = EWPCPeakSignal2D(com, peak_cluster1, peak_cluster2)

        # com.axes_manager.navigation_axes[0].name = "Beam Position (y)"
        # com.axes_manager.navigation_axes[1].name = "Beam position (x)"
        # for nav_axes, sig_axes in zip(
        #         self.axes_manager.navigation_axes,
        #         com.axes_manager.signal_axes):
        #     pst._copy_axes_object_metadata(nav_axes, sig_axes)
  
        return com

class EWPCPS_meta(type):
    def __repr__(cls):
        return 'StrainMAPPER.signals.EWPCPeakSignal2D'

class EWPCPeakSignal2D(metaclass=EWPCPS_meta):
    def __init__(self, com, peak_cluster1, peak_cluster2):
        self.data = com
        self.pc1 = peak_cluster1
        self.pc2 = peak_cluster2

    def __repr__(self):
        rep = 'EWPC subpixel Peak Array - {0} - dimensions: {1}\n'.format(type(self.data), self.data.shape)
        return rep

    def reference(self, ref_x, ref_y):
        """
        Calculate the reference signal from the given coordinates.

        Parameters
        ----------
        ref_x : int or list of ints
            The x-coordinate(s) of the reference region. If it's a list, it should contain 2 elements.
        ref_y : int or list of ints
            The y-coordinate(s) of the reference region. If it's a list, it should contain 2 elements.

        Returns
        -------
        ref : numpy.ndarray
            The reference signal calculated from the given coordinates.
            
        Raises
        ------
        RuntimeError
            If the length of `ref_x` or `ref_y` is not equal to 2 when `ref_x` or `ref_y` is a list.
        ValueError
            If the reference signal is all zeros or NaNs.

        Note
        ----
        The `marker` attribute of the instance is updated based on the input parameters.
        """

        if type(ref_x) is list and type(ref_y) is list:
            if len(ref_x) != 2 or len(ref_y) != 2:
                raise RuntimeError("The length of ref_x [{0}] and/or length of "
                                "ref_y [{1}] is longer than 2".format(len(ref_x), len(ref_y)))

            ref_x.sort()
            ref_y.sort()
            ref = np.average(self.data[:,:,ref_y[0]:ref_y[1]+1,ref_x[0]:ref_x[1]+1],axis = (-2,-1))
            self.marker = dict(xy = (ref_x[0], ref_y[0]), width = ref_x[1]-ref_x[0], height = ref_y[1]-ref_y[0],
                                linewidth=1, edgecolor='r', facecolor="none")
        else:
            ref = self.data[:,:,ref_y,ref_x]
            self.marker = dict(xy = (ref_x, ref_y), width = 1, height = 1, linewidth=1,
                         edgecolor='none', facecolor="r")

        if np.all(ref == 0) or np.all(ref.mask):
            raise ValueError('The referene can not be zero or NaN. Choose a different refference')
            
        print('The reference values are:\nC1_x: %0.2f\tC2_x: %0.2f\nC1_y: %0.2f\tC2_y: %0.2f'%(tuple(ref.flatten())))
        return ref

    def strain_matrix(self, ref, refCoords =True, car_basis = False, export = False):
        """
        Compute the strain and rotation matrices of a 2D image using a reference image.

        Parameters
        ----------
        ref : array_like
            A 2x2 array of the reference image for which the deformation of the input image is calculated.
        refCoords : bool, optional
            If True, the output strain matrices will be computed in the reference coordinate system (default is True).
        car_basis : bool, optional
            If True, the deformation gradient will be computed with respect to the cartesian basis vectors (default is False).
        export : bool, optional
            If True, the function returns the strain and rotation matrices instead of updating the object's attributes (default is False).

        Returns
        -------
        E_array : ndarray
            The computed strain matrices. If `export` is True, this will be returned instead of updating the object's attribute.
        theta_array : ndarray
            The computed rotation angles in degrees. If `export` is True, this will be returned instead of updating the object's attribute.

        Raises
        ------
        ValueError
            If the deformation matrix for a pixel is zero.

        Warnings
        --------
        UserWarning
            If the deformation matrix failed to converge for one or more pixels.

        Note
        ----
        This function computes the deformation gradient of an input 2D image with respect to a reference image. The deformation gradient is then decomposed into its polar decomposition, yielding the rotation and stretch tensors. The rotation tensor is used to calculate the angle of rotation, and the stretch tensor is used to calculate the strain tensor. 
        
            - If `refCoords` is True, the strain tensor is computed in the reference coordinate system, which is defined by the basis vectors of the reference image. 
            - If `car_basis` is True, the deformation gradient is computed with respect to the cartesian basis (x,y) of the input image. Else, the deformation gradient is computed with respect to the vector basis (v1,v2) created by the two selected EWPC peaks.       
            - If `export` is True, the function returns the computed strain and rotation matrices instead of updating the object's attributes.
            
        """

        dim_shape = self.data.shape[-2:]
        E_array = np.empty_like(self.data)
        theta_array = np.empty(dim_shape)
        fail_list = list()
        for yy,xx in np.ndindex(dim_shape):
            try:
                peak_vectors = self.data[:,:,yy,xx]
                if np.ma.is_masked(peak_vectors):
                    E = np.nan
                    theta = np.nan    
                else:
                    if car_basis:
                        # D, residuals, rank, singular_values = np.linalg.lstsq(ref,peak_vectors, rcond = None)
                        D = peak_vectors @ np.linalg.inv(ref)
                    else:
                        D = np.linalg.inv(ref) @ peak_vectors
                    
                    if np.all(D == 0):
                        raise ValueError('Error: Deformation matrix for (y:%i,x:%i) is zero'%(yy,xx))

                    R_matrix, U_matrix = polar(D)
                    V_matrix = polar(D, side = 'left')[1]
                    
                    if refCoords:
                        E = U_matrix - np.identity(2)
                    else:
                        E = V_matrix - np.identity(2)
                    
                    theta = 180*np.arctan2(R_matrix[1,0],R_matrix[0,0])/np.pi
                    
                E_array[:,:,yy,xx] = E
                theta_array[yy,xx] = theta
            except:
                E = np.nan
                theta = np.nan  
                fail_list.append(np.array([yy,xx]))
        if len(fail_list) != 0:
             _logger.warning('The deformation matrix failed to converge for one or more pixels!\n\n%s'%(fail_list))
        if export == False:
            self.strainE = E_array
            self.strainT = theta_array
        else:
            return E_array, theta_array

    def angle_matrix(self, radians = False, export = False):
        """
        Calculate the angle matrix for a set of peak vectors.

        Parameters
        ----------
        radians : bool, optional
            If True, return the angle matrix in radians. Otherwise, return in degrees.
            Default is False.
        export : bool, optional
            If True, return the angle matrix. Otherwise, store the angle matrix in 
            the attribute 'angleA' of the instance. Default is False.

        Returns
        -------
        numpy.ndarray or None
            If `export` is True, returns the angle matrix as a numpy.ndarray with 
            shape (N, M), where N and M are the dimensions of the input data. 
            Otherwise, returns None and stores the angle matrix in the attribute 
            'angleA' of the instance.

        Note
        ----
        The angle matrix is calculated as the angle between two peak vectors. 
        If the input data is masked, the corresponding value in the output angle 
        matrix will be NaN.
        """
        
        dim_shape = self.data.shape[-2:]
        angle_array = np.empty(dim_shape)
        for yy,xx in np.ndindex(dim_shape):
            peak_vectors = self.data[:,:,yy,xx].T
            if np.ma.is_masked(peak_vectors):
                angle = np.nan
            else:
                dot = np.dot(peak_vectors[0], peak_vectors[1])
                norm = (np.linalg.norm(peak_vectors[0]) * np.linalg.norm(peak_vectors[1]))
                angle = np.arccos(dot/norm)
                if not radians:
                    angle = np.rad2deg(angle)
            angle_array[yy,xx] = angle
        if export == False:
            self.angleA = angle_array
        else:
            return angle_array

    def plot(self, single = False, index = 0, **kwargs):
        """
        Plot the data in a 2D grid of images, where each image corresponds to a cluster
        of peaks. Alternatively, plot a single cluster image.
        
        Parameters
        ----------
        single : bool, optional
            If True, plot a single cluster image corresponding to the cluster index given
            by `index`. Otherwise, plot a grid of images for all clusters.
            Default is False.
        index : int, optional
            The index of the cluster to plot if `single` is True. Ignored if `single`
            is False. 
            
                - 0: C1_x [Default]
                - 1: C1_y
                - 2: C2_x
                - 3: C2_y
                
        **kwargs : dict
            Additional keyword arguments to be passed to the `imshow` function.
        
        Returns
        -------
        fig : Figure
            The matplotlib Figure object containing the plot.
        
        Note
        ----
        The input data is assumed to have shape (2, 2, M, N), where M and N are the
        dimensions of each peak image. The function will plot the images in a 2x2 grid,
        with each row corresponding to a different peak cluster and each column
        corresponding to a different axis (x or y).
        
        If `single` is True, the function will plot only one image, corresponding to
        the cluster index given by `index`.
        
        The tick labels and axes are removed from all plots, except for the x and y
        labels showing the pixel positions in each dimension.
        """
        
        shape = self.data.shape
        if single:
            fig, ax = plt.subplots()
            cluster_plot = ax.imshow(self.data[int(index%2), int(index/2)], interpolation='nearest', **kwargs)
            cbar = fig.colorbar(cluster_plot, ax=ax)
            cbar.set_label('pixel', rotation=270,  labelpad=10)
            ax.set_title('cluster %i (%s)'%(self.pc1 if int(index/2) == 0 else self.pc2, 'x' if (index%2) == 0 else 'y'))
        else: 
            fig, ax = plt.subplots(2,2)
            for xx,yy in np.ndindex((2,2)):
                axs = ax[yy,xx]
                cluster_plot = axs.imshow(self.data[yy,xx], interpolation='nearest', **kwargs)
                cbar = fig.colorbar(cluster_plot, ax=axs)
                cbar.set_label('pixel', rotation=270,  labelpad=10)
                axs.set_title('cluster %i (%s)'%(self.pc1 if xx == 0 else self.pc2, 'x' if yy == 0 else 'y'))

        for axx in fig.axes:
            if axx.get_label() != '<colorbar>':
                axx.tick_params(left = False, right = False , labelleft = False,
                                labelbottom = False, bottom = False)
                axx.set_xlabel('$r^p_x$')
                axx.set_ylabel('$r^p_y$')
        plt.tight_layout()
        return fig

    def plot_strain(self, E_array = None, theta_array = None, equal_scale = False, gaussian_blur = None, single = False, index = 0, **kwargs):
        """
        Plot the strain or rotation field of the image.

        Parameters
        ----------
        E_array : array_like or None, optional
            The array containing the strain field to be plotted. If None, the strain field stored in the class object is used. Default is None.
        theta_array : array_like or None, optional
            The array containing the rotation field to be plotted. If None, the rotation field stored in the class object is used. Default is None.
        equal_scale : bool, optional
            Whether to set the colorbar scale to be equal across all subplots. Default is False.
        gaussian_blur : float or None, optional
            The standard deviation of the Gaussian filter applied to the strain and rotation fields. If None, no filter is applied. Default is None.
        single : bool, optional
            Whether to plot the strain/rotation field in a single subplot or in four subplots. Default is False.
        index : int, optional
            The index of the strain component to be plotted.
            
                - 0: xx [Default]
                - 1: yy
                - 2: xy
                - 3: Theta 
                
        **kwargs
            Additional keyword arguments passed to the `imshow` function of matplotlib.

        Returns
        -------
        fig : Figure
            The matplotlib figure object containing the plot.
        """
        
        shape = self.data.shape 
        if E_array is None:
            E_array = self.strainE
        if theta_array is None:
            theta_array = self.strainT

        if gaussian_blur is not None:
            E_array = et.gaussian_blur(E_array, gaussian_blur, order = 0)
            theta_array = et.gaussian_blur(theta_array, gaussian_blur, order = 0)   
        
        # Plotting options
        if 'cmap' not in kwargs.keys():
            kwargs.update(cmap = custom_cmap())

        vmax = vmin = None
        if 'vmax' in kwargs.keys():
            vmax = kwargs['vmax']
            kwargs.pop('vmax')
        if 'vmin' in kwargs.keys():
            vmin = kwargs['vmin']
            kwargs.pop('vmin')

        if vmax is not None and vmin is not None:
            if vmin != -vmax:
                _logger.warning(('vmax [{0}] and vmin [{1}] are not the same. This is not possible with Centered Norm.'
                                'Therefore, vmin will be adjusted such that vmin = -vmax').format(vmax, vmin))
        
        if vmax is None and vmin is None:
            vmax_t =  np.nanmax(abs(theta_array))
            if equal_scale:
                vmax_array = np.nanmax(abs(E_array)) * np.ones((2,2))
            else:
                vmax_array = np.nanmax(abs(E_array), axis = (2,3))
        
        else:
            vmax_array = np.ones((2, 2)) * (vmax if vmax is not None else -vmin)
            vmax_t = vmax if vmax is not None else -vmin
            
        
        vmin_array = -vmax_array
        vmin_t = -vmax_t 
        
        if single:
            fig, ax = plt.subplots()
            if index == 3:
                sp = ax.imshow(theta_array[:,:],
                            norm = Normalize(vmin  = vmin_t, vmax = vmax_t),
                            interpolation='nearest', **kwargs)
                ax.set_title(r"$\theta$")
                cbar = plt.colorbar(sp, ax = ax)
                cbar.set_label('degrees ($\degree$)', rotation=270,  labelpad=10)
            else:
                E_coord = (int(index%2),int(np.ceil(index/2)))
                sp = ax.imshow(E_array[int(index%2),int(np.ceil(index/2))],
                                norm = Normalize(vmin=vmin_array[E_coord], vmax=vmax_array[E_coord]),
                                interpolation='nearest', **kwargs)
                ax.set_title(r"$\epsilon_{%s}$"%('xx' if index == 0 else 'yy' if index == 1 else 'xy'))
                cbar = plt.colorbar(sp, ax = ax)
                cbar.set_label('(%)', rotation=270,  labelpad=10)
                cbar.ax.yaxis.set_major_formatter('{x:.1%}')
        else:
            fig, ax = plt.subplots(2,2)
            for index in range(4):
                if index == 3:
                    sp = ax[1,1].imshow(theta_array[:,:],norm = CenteredNorm(),
                                        interpolation='nearest', **kwargs)
                    ax[1,1].set_title(r"$\theta$")
                    cbar = plt.colorbar(sp, ax = ax[1,1])
                    cbar.set_label('degrees ($\degree$)', rotation=270,  labelpad=10)
                else:
                    axs = ax[int(index/2), int(index%2)]
                    E_coord = (int(index%2),int(np.ceil(index/2)))
                    sp = axs.imshow(E_array[E_coord],
                                    norm = Normalize(vmin=vmin_array[E_coord], vmax=vmax_array[E_coord]),
                                    interpolation='nearest', **kwargs)
                    axs.set_title(r"$\epsilon_{%s}$"%('xx' if index == 0 else 'yy' if index == 1 else 'xy'))
                    cbar = plt.colorbar(sp, ax = axs)
                    cbar.ax.yaxis.set_major_formatter('{x:.1%}')
                    
        for axx in fig.axes:
            if axx.get_label() != '<colorbar>':
                axx.tick_params(left = False, right = False , labelleft = False,
                                labelbottom = False, bottom = False)
                axx.set_xlabel('$r^p_x$')
                axx.set_ylabel('$r^p_y$')

            axx.add_patch(patches.Rectangle(**self.marker))
        plt.tight_layout()
        return fig
    
    def plot_angle(self, angle_array = None, gaussian_blur = None, **kwargs,):
        if angle_array is None:
            angle_array = self.angleA

        if gaussian_blur is not None:
            angle_array = et.gaussian_blur(angle_array, gaussian_blur, order = 0)
        
        fig, ax = plt.subplots()
        plot = ax.imshow(angle_array, interpolation='nearest', **kwargs)
        cbar = plt.colorbar(plot, ax = ax)
        return fig

    def save(self):
        """
        Save the data and metadata of the current instance to a NumPy compressed (.npz) file.

        This method allows you to save the data and metadata of the current instance of
        `EWPCPeakSignal2D` to a NumPy compressed file (.npz). The saved file will include
        the data, mask, and peak cluster information, making it possible to later load
        and reconstruct the instance.

        Raises
        ------
        NoFileSelectedError
            If the user cancels the file save dialog or selects no files.
        FileNotFoundError
            If the selected save path does not exist.
        ValueError
            If there is an issue with saving the data and metadata to the file.
        """
        from StrainMAPPER.io_tools import save_dialog
        com = self.data
        save_path = save_dialog(filetypes = [('numpy compressed', '*.npz')] ,defaultextension = '.npz')[0]
        np.savez_compressed(save_path, data = com.data, mask = com.mask, pc1 = self.pc1, pc2 = self.pc2)


class EWPCPA_meta(type):
    def __repr__(cls):
        return 'StrainMAPPER.signals.EWCPeakArray'

class EWPCPeakArray(metaclass=EWPCPA_meta):
    def __init__(self, data, signal_shape):
        self.data = data
        self.signal_shape = signal_shape
        
    def __repr__(self):
        rep = 'EWPC Peak Array - {0} - dimensions: {1}\nSubarray - {2} - dimensions: {3}'.format(type(self.data), self.data.shape, 
                                                                          type(self.data[0,0]), self.data[0,0].shape)
        return rep

    def cluster_peaks(self, n_clusters, threshold = None, threshold_max = 1E99, sample_weight = True, **kwargs):
        """
        Cluster the peaks in the data using KMeans algorithm and visualize the results.

        Parameters
        ----------
        n_clusters : int
            Number of clusters to create.
        threshold : float, optional
            Only peaks with counts above this value will be used for clustering.
        threshold_max : float, optional
            Only peaks with counts below this value will be used for clustering.
        sample_weight : bool, optional
            Whether to use the peak counts as sample weights for clustering.
        **kwargs : dict
            Additional keyword arguments to pass to KMeans and plot_clusters functions.

        Returns
        -------
        cluster : KMeans
            Fitted KMeans object.

        Note
        ----
        This method flattens the peak array, trims the peaks based on threshold values,
        and then applies KMeans clustering algorithm to cluster the remaining peaks.
        The resulting clusters are then plotted using the plot_clusters function.
        """
        
        fig_kwargs = {}
        for kwds in ['background_kwds', 'scatter_kwds', 'image_kwds', 'background']:
            if kwds in kwargs.keys():
                fig_kwargs[kwds] = kwargs[kwds]
                del kwargs[kwds]

        peak_flat, peak_unique, peak_count = et.flatten_peak_array(self.data)
        all_count = len(peak_flat)
        if threshold is not None or threshold_max != 1E99:
            peak_unique, peak_count, peak_removed = et.trim_peak_array(peak_unique, peak_count, threshold, threshold_max)
            all_count = len(peak_flat)

        unique_count = len(peak_unique)
        print('found %i unqiue peaks out of %i [%0.2f%%]' %(unique_count, all_count, 100*(unique_count/all_count)))
        
        cluster = KMeans(n_clusters=n_clusters, **kwargs).fit(peak_unique, sample_weight = (peak_count if sample_weight else None))
        fig = et.plot_clusters(cluster, peak_unique, peak_count, self.signal_shape[-2:] , **fig_kwargs)
        plt.show()
        return cluster

    
    
    def threshold(self, threshold, threshold_max = 1E99, inplace = False):
        """
        Trims the peak array by removing peaks whose count is below the specified threshold or above the specified threshold_max.

        Parameters
        ----------
        threshold : int or float
            The minimum count required to keep a peak.
        threshold_max : int or float, optional (default=1E99)
            The maximum count to keep a peak. Peaks whose count exceeds this value will be removed.
        inplace : bool, optional (default=False)
            If True, the peak array is modified in-place. If False, a new peak array is returned.

        Returns
        -------
        EWPCPeakArray or None
            If inplace is True, returns None. Otherwise, returns a new EWPCPeakArray object containing
            the trimmed peak array.

        Note
        ----
        The threshold parameter specifies the minimum count required to keep a peak. Peaks whose count is
        less than the threshold will be removed. The threshold_max parameter specifies the maximum count
        to keep a peak. Peaks whose count exceeds this value will also be removed.

        If inplace is True, the peak array is modified in-place and None is returned. If inplace is False,
        a new EWPCPeakArray object is returned containing the trimmed peak array.
        """
        
        peak_unique, peak_count = et.flatten_peak_array(self.data)[1:]
        peak_removed = et.trim_peak_array(peak_unique, peak_count, threshold, threshold_max)[2]
        peak_removed = peak_removed.tolist()
        trim_array = np.copy(self.data)
        for yy,xx in np.ndindex(trim_array.shape):
            trim_peak = trim_array[yy,xx]
            trim_shape = trim_peak.shape
            delete = []
            for row in trim_peak:
                delete.append(row.tolist() in peak_removed)
            trim_peak = np.delete(trim_peak, delete, axis = 0)
            trim_peak.resize(trim_shape, refcheck = False)
            trim_array[yy,xx] = trim_peak

        if inplace:
            self.data = trim_array
        else:
            return EWPCPeakArray(trim_array, self.signal_shape)
    

class LazyEWPCSignal2D(LazySignal, EWPCSignal2D):
    _lazy = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compute(self, *args, **kwargs):
        super().compute(*args, **kwargs)
        self.__class__ = EWPCSignal2D
        self.__init__(**self._to_dictionary())

class LazyEWPCPeakSignal2D(LazySignal, EWPCPeakSignal2D):
    _lazy = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compute(self, *args, **kwargs):
        super().compute(*args, **kwargs)
        self.__class__ = EWPCPeakSignal2D
        self.__init__(**self._to_dictionary())

class LazyEWPCPeakArray(EWPCPeakArray):
    _lazy = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compute(self, *args, **kwargs):
        super().compute(*args, **kwargs)
        self.__class__ = EWPCPeakArray
        self.__init__(**self._to_dictionary())