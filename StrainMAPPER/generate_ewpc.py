# © StrainMAPPER developer team 2023
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import dask.array as da
import pixstem.pixelated_stem_tools as pst
from dask.diagnostics import ProgressBar
from StrainMAPPER.signals import EWPCSignal2D, LazyEWPCSignal2D
import StrainMAPPER.EWPC_tools as et

def generate_ewpc(pixelatedstem, use_filter=False, low_sigma=None, filter_kwds={}, lazy_result=False, show_progressbar=False, chunk_calculations=None):
    """
    Generate Exit Wave Power Cepstrum (EWPC) of a 2D Dask array from pixelated STEM data.

    Parameters
    ----------
    pixelatedstem : pixstem.PixelatedSTEM object
        PixelatedSTEM object containing the input data.
    use_filter : bool, optional
        Whether to apply a difference of Gaussians filter to the image before
        calculating the cepstrum. Default is False.
    low_sigma : float, optional
        The standard deviation of the Gaussian filter applied to the image.
        Required if `use_filter` is True.
    filter_kwds : dict, optional
        Additional arguments to be passed to the difference_of_gaussians function,
        if `use_filter` is True. Default is an empty dictionary.
    lazy_result : bool, optional
        Whether to return a LazyEWPCSignal2D object instead of an EWPCSignal2D object.
        Default is False.
    show_progressbar : bool, optional
        Whether to show a progress bar during computation. Default is False.
    chunk_calculations : list, optional
        List of chunk sizes for each dimension of the Dask array. If not specified, 
        a default value of 16 chunks per navigation dimension is used.

    Returns
    -------
    EWPC : StrainMAPPER.signals.EWPCSignal2D or StrainMAPPER.signals.LazyEWPCSignal2D
        An EWPCSignal2D or LazyEWPCSignal2D object, depending on the value of `lazy_result`.
        The EWPC is calculated as follows:
            1. The image is log-transformed.
            2. If `use_filter` is True, a difference of Gaussians filter is applied
               to the image using `low_sigma` and any additional keyword arguments in `filter_kwds`.
            3. The image is Fourier transformed and the absolute value of the
               result is taken.
            4. The result is shifted so that the origin is at the center of the image.
            5. The log-transformed magnitude of the Fourier transform is calculated
               and stored in the corresponding pixel of EWPC.

    Raises
    ------
    ValueError
        If `use_filter` is True but `low_sigma` is not specified.

    """
    
    det_shape = pixelatedstem.axes_manager.signal_shape
    nav_dim = pixelatedstem.axes_manager.navigation_dimension
    if chunk_calculations is None:
        chunk_calculations = [16] * nav_dim + list(det_shape)
    if pixelatedstem._lazy:
        dask_array = pixelatedstem.data.rechunk(chunk_calculations)
    else:
        dask_array = da.from_array(pixelatedstem.data, chunks=chunk_calculations)
    
    if use_filter and low_sigma is None:
        raise ValueError('low_sigma [{0}] is not specified, please specify a standard deviation'.format(low_sigma))
    
    EWPC = da.map_blocks(et.Exit_wave_power_cepstrum, dask_array, use_filter, low_sigma, **filter_kwds)
    EWPC = EWPC.astype(np.float64)

    if lazy_result:
        EWPC = LazyEWPCSignal2D(EWPC)
    else:
        if show_progressbar:
            pbar = ProgressBar()
            pbar.register()
        EWPC = EWPC.compute()
        if show_progressbar:
            pbar.unregister()
        EWPC = EWPCSignal2D(EWPC)

    for nav_axes, sig_axes in zip(
            pixelatedstem.axes_manager.navigation_axes,
            EWPC.axes_manager.navigation_axes):
        pst._copy_axes_object_metadata(nav_axes, sig_axes)

    for nav_axes, sig_axes in zip(
            pixelatedstem.axes_manager.signal_axes,
            EWPC.axes_manager.signal_axes):
        pst._copy_axes_object_metadata(nav_axes, sig_axes)

    EWPC.metadata.add_dictionary(pixelatedstem.metadata.as_dictionary())

    return EWPC