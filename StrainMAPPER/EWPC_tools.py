# © StrainMAPPER developer team 2023
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import dask.array as da
from sklearn.cluster import KMeans
from skimage.filters import difference_of_gaussians
from skimage.feature import blob_dog, blob_log, blob_doh
from scipy import ndimage
from pixstem.pixelated_stem_tools import _make_circular_mask
from pixstem.dask_tools import _threshold_array
from matplotlib.colors import Normalize, CenteredNorm, LogNorm
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge
import pkg_resources

def hanning_filtering_window(N_x, N_y):
    """
    Return a 2D Hanning window with the shape (N_y, N_x).
    The Hanning window is a taper formed by using a weighted cosine. 
    
    Parameters
    ----------
    N_x : int
        Number of points in the x-axis for the output window
    N_y : int
        Number of points in the y-axis for the output window
        
    Returns
    -------
     : ndarray, shape(N_y,N_x)
        The window, with the maximum value normalized to one.
    
    """
    
    wc = np.hanning(N_x)
    wr = np.hanning(N_y)
    wc, wr = np.meshgrid(wc, wr)
    mask = [wc,wr]
    window = mask[0]*mask[1]
    return window

def flatten_peaks(peak_array):
    """
    Flatten a nested 2D numpy array of peak locations to a 2D array of (x, y) coordinates.

    Parameters
    ----------
    peak_array : ndarray
        A nested 2D numpy array containing (x, y) coordinates of peak locations.
        The outer array contains peak arrays of varying sizes, while each peak array
        has shape (n, 2), where n is the number of peaks.

    Returns
    -------
    peak_flat : ndarray
        A 2D array of shape (N, 2) containing the same (x, y) coordinates as `peak_array`,
        but flattened to a single axis, where N is the total number of peaks.

    Examples
    --------
    >>> import numpy as np
    >>> peak_array = np.array([[[1, 2], [3, 4]], [[5, 6], [7, 8], [9, 10]]])
    >>> flatten_peaks(peak_array)
    array([[ 1,  2],
        [ 3,  4],
        [ 5,  6],
        [ 7,  8],
        [ 9, 10]])

    Note
    ----
    The input `peak_array` is first converted to a Python list, and then to a numpy array,
    to ensure that it has the correct data type and shape. If the shape of `peak_flat`
    does not match that of `peak_array`, the function assumes that the original array
    was not padded with zeros, and reshapes `peak_flat` accordingly.
    """
    
    peak_flat = peak_array.tolist()
    peak_flat = np.array(peak_flat)
    peak_shape = peak_flat.shape
    if peak_shape != peak_array.shape:
        peak_flat = peak_flat.reshape(peak_shape[0]*peak_shape[1]*peak_shape[2],2)
    return peak_flat

# def cluster_peaks(peak_array, n_clusters):
#     """
#     Clusters peaks in a data set using the KMeans algorithm.

#     Args:
#     peak_array: The peak data, as a numpy array or dask array.
#     n_clusters: The number of clusters to use in the algorithm.

#     Returns:
#         kmeans object
#         the peak cluster data, and the peak count data.
#     """
#     peak_flat = flatten_peaks(peak_array)
#     peak_flat = np.delete(peak_flat, np.where(peak_flat==([[0,0]])), axis = 0)
#     peak_unique, peak_count = np.unique(peak_flat, return_counts= True, axis=0)
#     kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(peak_unique, sample_weight=peak_count)
#     return kmeans, peak_unique, peak_count

def select_cluster(peak_array, kmeans, peak_cluster):
    """
    Select a subset of peaks belonging to a specific cluster using a KMeans object.

    Parameters
    ----------
    peak_array : ndarray
            A nested 2D numpy array containing (x, y) coordinates of peak locations.
            The outer array contains peak arrays of varying sizes, while each peak array
            has shape (n, 2), where n is the number of peaks.
    kmeans : KMeans
        A KMeans object fitted on `peak_array`.
    peak_cluster : int
        An integer specifying the cluster number to select.

    Returns
    -------
    trim_array : ndarray
            A nested 2D numpy array containing (x, y) coordinates of peak locations.
            Similar to the input but only containing peaks that belong to the specified cluster. 
            The outer array contains peak arrays of varying sizes, 
            while each peak array has shape (n, 2), where n is the number of peaks.

    Examples
    --------
    >>> import numpy as np
    >>> from sklearn.cluster import KMeans
    >>> peak_array = np.array([[1, 2], [3, 4], [5, 6], [7, 8], [9, 10]])
    >>> kmeans = KMeans(n_clusters=2).fit(peak_array)
    >>> select_cluster(peak_array, kmeans, 1)
    array([[3, 4],
           [5, 6]])

    Note
    ----
    This function selects a subset of peaks from `peak_array` that belong to a specific
    cluster identified by `peak_cluster`, using a KMeans object `kmeans` that has been
    previously fitted on `peak_array`.

    For each peak in `peak_array`, the function checks if it belongs to the cluster
    specified by `peak_cluster` using the `predict` method of `kmeans`. If the peak
    belongs to the cluster, it is retained in `trim_array`. Otherwise, it is deleted
    using the `np.delete` function, and a new 2D numpy array is created with the
    remaining peaks using `numpy.ndarray.resize`. The resulting array is stored in `trim_array`,
    which is returned by the function.

    This function assumes that `peak_array` is a 2D numpy array containing (x, y)
    coordinates of peak locations, and that `kmeans` is a KMeans object fitted on
    `peak_array`. If `peak_array` is empty, the function returns an empty array.
    """

    trim_array = np.copy(peak_array)
    for yy,xx in np.ndindex(trim_array.shape):
        k_label = kmeans.predict(trim_array[yy,xx]) != peak_cluster
        trim = np.delete(trim_array[yy,xx], k_label, axis = 0)
        trim.resize((1,2))
        trim_array[yy,xx] = trim
    return trim_array

def save_peak_array(filepath, peak_array, n_clusters=-1, r=-1, r_outer=-1, min_sigma=1,
                     max_sigma=-1, num_sigma=-1, sigma_ratio=-1, threshold=-1, log_scale=-1):
    """
    Save peak array and associated parameters to a compressed numpy archive.

    Parameters
    ----------
    filepath : str
        The path of the output file to save the peak array and variables.
    peak_array : ndarray
        A nested 2D numpy array containing (x, y) coordinates of peak locations.
        The outer array contains peak arrays of varying sizes, while each peak array
        has shape (n, 2), where n is the number of peaks.
    n_clusters : int, optional
        The number of clusters used to fit the peak array, default is -1.
    r : int, optional
        The radius of the circular mask used to crop the peak array, default is -1.
    r_outer : int, optional
        The outer radius of the annular mask used to crop the peak array, default is -1.
    min_sigma : float, optional
        The minimum standard deviation of the Gaussian kernel used for blob detection, default is 1.
    max_sigma : float, optional
        The maximum standard deviation of the Gaussian kernel used for blob detection, default is -1.
    num_sigma : int, optional
        The number of standard deviations between `min_sigma` and `max_sigma` used for blob detection, default is -1.
    sigma_ratio : float, optional
        The ratio between the standard deviation of the Gaussian kernel for the difference of Gaussians and that for the Laplacian of Gaussians, default is -1.
    threshold : float, optional
        The threshold value used to detect blobs, default is -1.
    log_scale : bool, optional
        Whether to use a logarithmic color scale for the peak array plot, default is -1.

    Examples
    --------
    >>> import numpy as np
    >>> peak_array = np.array([[1, 2], [3, 4], [5, 6], [7, 8], [9, 10]])
    >>> save_peak_array('peak_array.npz', peak_array, n_clusters=3, r=5, threshold=0.5)

    Note
    ----
    This function saves a 2D numpy array of peak locations `peak_array` and associated
    parameters to a compressed numpy archive at `filepath`. The optional parameters are
    saved as a numpy structured array called `variables`, with named fields matching
    the parameter names.

    If a parameter is not provided, its default value is used instead. The default value
    for each parameter is given in the function signature.

    This function assumes that `filepath` is a valid path string, and that `peak_array`
    is a 2D numpy array containing (x, y) coordinates of peak locations.

    The resulting numpy archive can be loaded using the `numpy.load` function.
    """

    variables = np.array([(n_clusters, r, r_outer, min_sigma, max_sigma, num_sigma, sigma_ratio, threshold, log_scale)],
                        dtype = [('n_clusters', int),
                                 ('r', int),
                                 ('r_outer', int),
                                 ('min_sigma',float),  
                                 ('max_sigma', float), 
                                 ('num_sigma', int),
                                 ('sigma_ratio', float), 
                                 ('threshold', float),
                                 ('log_scale', bool)])
    np.savez_compressed(filepath,  peak_array = peak_array, variables = variables)

def load_peak_array(filepath):
    """
    Loads a peak array and associated variables from a compressed numpy file (.npz).

    Parameters
    ----------
    filepath : str
        The path to the compressed numpy file.

    Returns
    -------
    peak_array : numpy.ndarray
        A nested 2D numpy array containing (x, y) coordinates of peak locations.
        The outer array contains peak arrays of varying sizes, while each peak array
        has shape (n, 2), where n is the number of peaks.

    variables : numpy.ndarray
        An array of variables used to generate the peak array, as saved by the
        `save_peak_array` function. The variables are stored in a structured array
        with the following fields:
            - 'n_clusters': int
            - 'r': int
            - 'r_outer': int
            - 'min_sigma': float
            - 'max_sigma': float
            - 'num_sigma': int
            - 'sigma_ratio': float
            - 'threshold': float
            - 'log_scale': bool

    """
    
    load = np.load(filepath, allow_pickle=True)
    peak_array = load['peak_array']
    variables = load['variables']
    return peak_array, variables

def Exit_wave_power_cepstrum(dask_array, use_filter, low_sigma, **kwargs):
    """
    Calculates the Exit Wave Power Cepstrum of a 2D Dask array.

    Parameters
    ----------
    dask_array : dask.array.core.Array
        A 2D Dask array containing intensity values.
    use_filter : bool
        Whether to apply a difference of Gaussians filter to the image before
        calculating the cepstrum.
    low_sigma : float
        The standard deviation of the Gaussian filter applied to the image.
    **kwargs : keyword arguments
        Additional arguments to be passed to the difference_of_gaussians function,
        if use_filter is True.

    Returns
    -------
    EWPC : numpy.ndarray
        A numpy array of the same shape as dask_array, containing the Exit Wave Power
        Cepstrum of each pixel. The cepstrum is calculated as follows:
            1. The image is log-transformed.
            2. If use_filter is True, a difference of Gaussians filter is applied
               to the image using low_sigma and any additional keyword arguments.
            3. The image is Fourier transformed and the absolute value of the
               result is taken.
            4. The result is shifted so that the origin is at the center of the image.
            5. The log-transformed magnitude of the Fourier transform is calculated
               and stored in the corresponding pixel of EWPC.

    """
    
    nav_dim = dask_array.shape[:2]
    EWPC = np.empty_like(dask_array)
    for yy, xx in np.ndindex(nav_dim):
        image = dask_array[yy,xx]
        image[image <= 0] = 1E-9
        image = np.log(image)
        if use_filter:
            image = difference_of_gaussians(image, low_sigma, **kwargs)
        image = np.fft.fft2(image*hanning_filtering_window(*image.shape))
        image = np.abs(np.fft.fftshift(image))        
        EWPC[yy,xx] = image
    return EWPC


def peak_find(dask_array, n_clusters, **kwargs):
    """
    Find the peak locations in a 4D dask array using the EWPC algorithm.

    Parameters
    ----------
    dask_array : dask.array.core.Array
        A 4D dask array containing electron microscopy data.
    n_clusters : int
        The number of peaks to be found in each frame of the dask array.
    **kwargs
        Keyword arguments passed to the `peak_find_single_frame` function.

    Returns
    -------
    peak_array : numpy.ndarray
        A nested 2D numpy array containing (x, y) coordinates of peak locations.
        The outer array contains peak arrays of varying sizes, while each peak array
        has shape (n_clusters, 2), where n_clusters is the number of peaks.

    Raises
    ------
    TypeError
        If the `peak_mode` argument passed to the `peak_find_single_frame` function
        is not one of 'LoG', 'DoG', or 'DoH'.

    Examples
    --------
    >>> dask_array = dask.array.random.normal(size=(10, 10, 128, 128))
    >>> peak_array = peak_find(dask_array, 5, peak_mode='DoG', r=5, r_outer=15)

    """
    
    peak_array = np.zeros(dask_array.shape[:2], dtype='object')
    for yy, xx in np.ndindex(dask_array.shape[:2]):
        EWPC_pixel = dask_array[yy, xx, :, :]
        EWPC_pixel_log = peak_find_single_frame(EWPC_pixel, n_clusters, **kwargs)[:,:2]
        peak_array[yy, xx] = EWPC_pixel_log[:n_clusters, :]
    return peak_array


def peak_find_single_frame(EWPC, n_clusters, peak_mode='DoG', r=5, r_outer=15, **kwargs):
    """
    Find peaks in a single frame of EWPC data using a specified peak detection method.

    Parameters
    ----------
    EWPC : numpy.ndarray
        The EWPC data for a single frame, with shape (n_rows, n_cols).
    n_clusters : int
        The maximum number of peaks to detect.
    peak_mode : str
        The peak detection method to use. Supported values are 'LoG' (Laplacian of Gaussian), 'DoG' (Difference of Gaussian), and 'DoH' (Determinant of Hessian). Defaults to 'DoG'.
    r : int
        The minimum radius (in pixels) from the center of the image where peaks are allowed to be detected. Defaults to 5.
    r_outer : int 
        The maximum radius (in pixels) from the center of the image where peaks are allowed to be detected. Defaults to 15.
    **kwargs : 
        Additional keyword arguments to be passed to the peak detection function.

    Returns
    -------
     : numpy.ndarray
        An array of peak locations, with shape (n_clusters, 2).
    """
    
    # ...

    if peak_mode == 'LoG' or peak_mode == 'log':
        peak_mode = blob_log
    elif peak_mode == 'DoG' or peak_mode == 'dog':
        peak_mode = blob_dog
    elif peak_mode == 'DoH' or peak_mode == 'hog':
        peak_mode = blob_doh
    else:
        raise TypeError('peak_mode ["{0}"] should be any of the following "LoG", "DoG", "DoH"'.format(peak_mode))

    max_shape = max(EWPC.shape)
    exclude_border = int((max_shape/2)-r_outer)
    EWPC_log = peak_mode(EWPC, **kwargs)
    delete_outside = np.sqrt((EWPC_log[:,0]-64)**2+(EWPC_log[:,1]-64)**2)>r_outer
    delete_inside = np.sqrt((EWPC_log[:,0]-64)**2+(EWPC_log[:,1]-64)**2)<r
    EWPC_log = np.delete(EWPC_log, delete_outside+delete_inside, axis = 0)#delete peaks outside mask
    # EWPC_log = np.delete(EWPC_log, 2, axis = 1)#delete column with sigma values
    EWPC_log.resize((n_clusters,3))
    return EWPC_log

def make_circular_4D_mask_chunck(centerX, centerY, image_size, radius):
    """
    Generate a 4D binary mask of shape `image_size` centered at the given `(centerX, centerY)`
    coordinates and with circular masks of radius `radius`. 

    Parameters
    ----------
    centerX : numpy.ndarray of shape (M, N)
        A 2D array of the x-coordinates of the center of the circular masks in each pixel.

    centerY : numpy.ndarray of shape (M, N)
        A 2D array of the y-coordinates of the center of the circular masks in each pixel.

    image_size : tuple of ints
        The shape of the output mask. Must be a 4D array with shape (M, N, H, W).

    radius : float
        The radius of the circular masks, in pixels.

    Returns
    -------
    mask_array : numpy.ndarray of shape (M, N, H, W)
        A binary mask where True indicates the pixel belongs to a circular mask and False
        otherwise.

    Note
    ----
    This function internally calls `_make_circular_mask()` from `pixstem.pixelated_stem_tools`
    to create a circular binary mask in 2D for each pixel in the `(M, N)` grid.

    """
    
    mask_array = np.zeros(image_size, dtype=bool)
    for yy, xx in np.ndindex(image_size[:2]):
        mask_array[yy, xx, :, :] = _make_circular_mask(centerX[yy, xx], centerY[yy, xx],
                                                       image_size[3], image_size[2], radius)
    return mask_array


from pixstem.pixelated_stem_tools import _make_circular_mask

def make_circular_4D_mask(centerX, centerY, image_size, radius, chunk_calculations = None):
    """
    Generate a 4D mask array of the same shape as `image_size`, with circular masks at each 2D location given by `centerX` and `centerY`. The radius of the circular mask is defined by `radius`.
    
    Parameters
    ----------
    centerX : array_like
        2D array with x-coordinates of the center of the circular masks for each 2D slice.
    centerY : array_like
        2D array with y-coordinates of the center of the circular masks for each 2D slice.
    image_size : tuple
        A 4D tuple with the shape of the 4D image array, with the last two dimensions being the dimensions of the 2D slices.
    radius : float
        The radius of the circular mask.
    chunk_calculations : tuple, optional
        The chunk size for calculating the 4D mask array. The default is None, which generates a chunk size of ((16,16))+image_size[-2:].
        
    Returns
    -------
    mask_array : Dask array
        A Dask array with the same shape as `image_size` containing circular masks at each location given by `centerX` and `centerY`.
           
    Raises
    ------
    ValueError
        If `image_size` is not 4D or `centerX` and `centerY` do not have the same shape as the navigation dimensions of `image_size`.
    
    See Also
    --------
    `_make_circular_mask()` from `pixstem.pixelated_stem_tools` : Function used to generate 2D circular masks.    
    """
    
    if len(image_size) !=4:
        raise ValueError("image_size ({0}) has ({1}) dimension(s) instead of 4 "
                         .format(image_size, len(image_size)) )
    
    if chunk_calculations is None:
        chunk_calculations = ((16,16))+image_size[-2:]
    
    if centerX.shape != image_size[:2] or centerY.shape != image_size[:2]:
        raise ValueError('centerX [{0}] and\or centerY [{1}] does not have the same shape '\
                        'as the navigation dimension of image_size [{2}]'.format(centerX.shape, centerY.shape, image_size[:2]))

    mask_array = da.map_blocks(make_circular_4D_mask_chunck, centerX, centerY, image_size, radius, chunks = image_size, dtype = bool)
    mask_array = mask_array.rechunk(chunk_calculations)
    return mask_array


def _center_of_mass_array(dask_array, threshold_value=None, mask_array=None):
    """
    Calculates the center of mass of a 4D Dask array along the last two dimensions.

    Parameters
    ----------
    dask_array : dask.array
        The 4D Dask array to calculate the center of mass from.
    threshold_value : float or None, optional
        The threshold value to use for thresholding the input array. Default is None, which means no thresholding is done.
    mask_array : ndarray or None, optional
        A 2D or 4D ndarray that serves as a mask for the input array. If a 2D ndarray is given, it will be broadcasted
        to the shape of the last two dimensions of the input array. If a 4D ndarray is given, it must have the same shape
        as the input array. Default is None, which means no masking is done.

    Returns
    -------
    beam_shifts : dask.array
        A 2D Dask array of the calculated center of mass for each image in the input array.

    Raises
    ------
    ValueError
        If the shape of mask_array does not match the shape of the last two dimensions of the input array.

    Note
    ----
    This function is based on the `_center_of_mass_array` function from `pixstem.dask_tools`.

    """

    det_shape = dask_array.shape[-2:]
    x = da.linspace(0,det_shape[1]-1,det_shape[1])
    y = da.linspace(0,det_shape[0]-1,det_shape[0])
    x_grad, y_grad = da.meshgrid(x, y)
    y_grad, x_grad = y_grad.astype(np.float64), x_grad.astype(np.float64)
    sum_array = da.ones_like(x_grad)

    if mask_array is not None:
        if mask_array.shape == det_shape or mask_array.shape == dask_array.shape:
            x_grad = x_grad * da.invert(mask_array)
            y_grad = y_grad * da.invert(mask_array)
            sum_array = sum_array * da.invert(mask_array)
        else:            
            raise ValueError(
                "mask_array ({0}) must have same shape as last two "
                "dimensions of the dask_array ({1})".format(
                    mask_array.shape, det_shape))

    if threshold_value is not None:
        dask_array = _threshold_array(
            dask_array, threshold_value=threshold_value,
            mask_array=mask_array)

    x_shift = da.multiply(dask_array, x_grad, dtype=np.float64)
    y_shift = da.multiply(dask_array, y_grad, dtype=np.float64)
    sum_array = da.multiply(dask_array, sum_array, dtype=np.float64)

    x_shift = da.sum(x_shift, axis=(-2, -1), dtype=np.float64)
    y_shift = da.sum(y_shift, axis=(-2, -1), dtype=np.float64)
    sum_array = da.sum(sum_array, axis=(-2, -1), dtype=np.float64)

    beam_shifts = da.stack((x_shift, y_shift))
    beam_shifts = da.divide(beam_shifts[:], sum_array, dtype=np.float64)
    return beam_shifts

def flatten_peak_array(peak_array):
    """
    Flattens a peak array and returns unique peaks and their counts.

    Parameters
    ----------
    peak_array : ndarray
        A peak array with shape (n_rows, n_columns, n_peaks, 2), where the last
        dimension contains the (x, y) coordinates of each peak.

    Returns
    -------
    peak_flat : ndarray
        A flattened peak array with shape (n_peaks, 2).
    peak_unique : ndarray
        A 2D array of unique peaks with shape (n_unique_peaks, 2).
    peak_count : ndarray
        An array containing the number of occurrences of each unique peak
        with shape (n_unique_peaks,).

    """
    
    peak_flat = flatten_peaks(peak_array)
    peak_flat = np.delete(peak_flat, np.where(peak_flat==([[0,0]])), axis = 0)
    peak_unique, peak_count = np.unique(peak_flat, return_counts= True, axis=0)
    return peak_flat, peak_unique, peak_count

def trim_peak_array(peak_unique, peak_count, threshold, threshold_max):
    """
    Trim the peak_unique and peak_count arrays based on count thresholds.
    
    Parameters
    ----------
    peak_unique : numpy.ndarray
        A 2D array of unique peak positions.
    peak_count : numpy.ndarray
        A 1D array of counts of unique peaks.
    threshold : int
        A minimum count threshold for keeping a peak.
    threshold_max : int
        A maximum count threshold for keeping a peak.
    
    Returns
    -------
    peak_unique_trimmed : numpy.ndarray
        A 2D array of unique peak positions, trimmed based on count thresholds.
    peak_count_trimmed : numpy.ndarray
        A 1D array of counts of unique peaks, trimmed based on count thresholds.
    peak_removed : numpy.ndarray
        A 2D array of peak positions that were removed based on count thresholds.
    """
    
    peak_removed = peak_unique[(peak_count < threshold) + (peak_count > threshold_max)]
    peak_unique_trimmed = peak_unique[(peak_count >= threshold) & (peak_count <= threshold_max)]    
    peak_count_trimmed = peak_count[(peak_count >= threshold) & (peak_count <= threshold_max)]
    return peak_unique_trimmed, peak_count_trimmed, peak_removed


def plot_clusters(cluster, peak_unique, peak_count, signal_shape, background=None, background_kwds={}, 
                  scatter_kwds={}, image_kwds={}):
    """
    Plot the clusters of peaks and their count, overlaid on the Kmeans array.

    Parameters
    ----------
    cluster : sklearn.cluster.KMeans
        Fitted KMeans cluster object
    peak_unique : numpy.ndarray
        Array of unique peak positions, obtained from flatten_peak_array
    peak_count : numpy.ndarray
        Array of peak count corresponding to peak_unique, obtained from flatten_peak_array
    signal_shape : tuple
        Shape of the 2D signal array
    background : numpy.ndarray, optional
        2D array for the background, by default None
    background_kwds : dict, optional
        Keyword arguments to be passed to the ax.imshow() method for the background, by default {}
    scatter_kwds : dict, optional
        Keyword arguments to be passed to the ax.scatter() method, by default {}
    image_kwds : dict, optional
        Keyword arguments to be passed to the ax.imshow() method for the Kmeans array, by default {}

    Returns
    -------
    matplotlib.figure.Figure
        The plotted figure

    Note
    ----
    The keyword argument 'cmap' and 'norm' in background_kwds will be set to 'gray' and LogNorm() if not provided.
    """

    if 'cmap' not in background_kwds.keys():
        background_kwds.update(cmap = 'gray')
    if 'norm' not in background_kwds.keys():
        background_kwds.update(norm = LogNorm())
        
    Kmeans_array = kmeans_array(cluster, signal_shape)
    fig, ax = plt.subplots()
    plt.tight_layout()
    ax.set_axis_off()
    if background is not None:
        ax.imshow(background, **background_kwds)
        if 'alpha' not in image_kwds.keys():
            image_kwds.update(alpha = 0.3)

    scatter = ax.scatter(peak_unique[:,1], peak_unique[:,0], c =peak_count, marker='.', **scatter_kwds)
    plt.colorbar(scatter, ax = ax)
    ax.imshow(Kmeans_array, **image_kwds)
    if image_kwds.get('labels', True):
        for n , (yy,xx) in enumerate(cluster.cluster_centers_):
            ax.text(xx,yy, n, c = 'w')
    return fig

def kmeans_array(kmeans, shape):
    """
    Predict cluster labels for each point in a grid and reshape the output to an array of the specified shape.

    Parameters
    ----------
    kmeans : sklearn.cluster.KMeans
        The k-means clustering object fitted on the data.
    shape : tuple
        The desired shape of the output array.

    Returns
    -------
    Kmeans_array : ndarray
        An array of the specified shape containing cluster labels for each point in a grid.
    """
    
    k_rows, k_cols = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), indexing='ij')
    k_indices = list(zip(k_rows.flatten(), k_cols.flatten()))
    Kmeans_array = kmeans.predict(k_indices)
    Kmeans_array = Kmeans_array.reshape(shape)
    return Kmeans_array


def plot_single_EWPC(EWPC, n_clusters, peak_mode='DoG', r=5, r_outer=15, plot_mask=False, plot_radius = False, image_kwds={}, **kwargs):
    """
    Plots a single frame of EWPC data with the detected peaks overlayed.

    Parameters
    ----------
    EWPC : array_like
        2D array of the EWPC data to be plotted.
    n_clusters : int
        The number of clusters to be detected.
    peak_mode : str, optional
        The method used for peak detection. Default is 'DoG'.
    r : int, optional
        The radius of the inner circle used to mask the image. Default is 5.
    r_outer : int, optional
        The radius of the outer circle used to mask the image. Default is 15.
    plot_mask : bool, optional
        Whether to plot the circular mask used to filter the data. Default is False.
    image_kwds : dict, optional
        Additional keyword arguments to pass to the imshow function for the EWPC data.
    **kwargs : dict, optional
        Additional keyword arguments to pass to the `peak_find_single_frame` function.

    Returns
    -------
    fig : matplotlib.figure.Figure
        The created figure object.
    """

    peak_log = peak_find_single_frame(EWPC, n_clusters, peak_mode = peak_mode, r = r, r_outer = r_outer, **kwargs)
    fig, ax = plt.subplots()
    plt.tight_layout()
    ax.set_axis_off()

    if 'norm' not in image_kwds.keys():
        image_kwds.update(norm = LogNorm())
    if 'cmap' not in image_kwds.keys():
        image_kwds.update(cmap = 'gray')

    ax.imshow(EWPC, **image_kwds)
    ax.scatter(peak_log[:,1], peak_log[:,0], c ='r', marker='.')
    if plot_mask:
        draw_donut = Wedge((EWPC.shape[1]/2,EWPC.shape[0]/2), r_outer, 0,360, width = r_outer-r, alpha=0.3, color = 'blue', visible = True)
        ax.add_artist(draw_donut)

    if plot_radius:
        for blob in peak_log:
            y, x, r = blob
            r *= np.sqrt(2)
            c = plt.Circle((x, y), r, color='red', linewidth=1, fill=False)
            ax.add_patch(c)

    return fig

def gaussian_blur(image, sigma, order, **kwargs):
    """
    Apply a Gaussian blur to an image.

    Parameters
    ----------
    image : array_like
        Input image to be blurred.
    sigma : scalar or sequence of scalars
        Standard deviation of the Gaussian kernel along each axis. A single scalar applies the same
        value along all axes, while a sequence applies a different value along each axis.
    order : int or sequence of ints, optional
        The order of the filter along each axis. An order of 0 corresponds to convolution with a Gaussian
        kernel. A positive order corresponds to convolution with that derivative of a Gaussian.
    **kwargs : dict, optional
        Optional keyword arguments passed to `scipy.ndimage.gaussian_filter`.

    Returns
    -------
    blurred_image : ndarray
        The Gaussian-blurred version of the input image, with NaN values masked out.

    Note
    ----
    Minimum Requirement: scipy => 1.11.0
    This function applies a Gaussian blur to the input image using `scipy.ndimage.gaussian_filter`.
    NaN values are masked out in the output image. 
    """
    class PackageVersionError(Exception):
        pass

    required_version = "1.11.0"

    try:
        installed_version = pkg_resources.get_distribution("scipy").version
    except pkg_resources.DistributionNotFound:
        installed_version = None

    if installed_version is None or pkg_resources.parse_version(installed_version) < pkg_resources.parse_version(required_version):
        raise PackageVersionError(f"The installed version of scipy ({installed_version}) is lower than the required version ({required_version}). Unable to calculate the Gaussian blur. Please upgrade scipy => 1.11.0")

    image_mask = np.isnan(image).data
    blur_image = np.copy(image)
    blur_image[np.isnan(blur_image)] = 0
    if np.ma.is_masked(image):
        image_mask += image.mask
        blur_image[image.mask == 1] = 0

    dim = len(blur_image.shape)
    if dim == 2:
        axes = None
    else:
        axes = (2,3)

    blur_image = ndimage.gaussian_filter(blur_image, axes=axes, sigma = sigma, order = order, **kwargs)
    blur_image = np.ma.masked_array(blur_image, mask = image_mask)
    return blur_image

def iEWPC(image):
    """
    Compute the Inverse Exit-Wave Power Cepstrum (iEWPC).
   
    1. Apply the inverse 2D Fast Fourier Transform (FFT) to the input image.
    2. Shift the zero-frequency component to the center of the image.
    3. Compute the absolute value of the shifted FFT image.
    4. Multiply the absolute image with a Hanning filtering window of the same shape as the image.
    5. Apply an element-wise exponential function (e^x) to the filtered image.

    Parameters
    ----------
    image : ndarray
        The input image (EWPC) for which the iEWPC will be computed.

    Returns
    -------
    image : ndarray
        The iEWPC image after applying the function.
    """
    image = np.fft.ifft2(image)
    image = np.fft.ifftshift(image)
    image = np.abs(image)
    image = image*hanning_filtering_window(*image.shape)
    image = np.e**(image)
    return image

def normalize_array(data):
    """
    Normalize the values in a NumPy array to the range [0, 1].

    This function takes a NumPy array `data` and scales its values to fall within the range [0, 1].
    The normalization is performed using the following formula:
    
    normalized_data = (data - min(data)) / (max(data) - min(data))

    Parameters
    ----------
    data : ndarray
        The input NumPy array to be normalized.

    Returns
    -------
    normalized_data : ndarray
        The normalized array with values in the range [0, 1].
    """
    min_data = np.min(data)
    max_data = np.max(data)
    return (data-min_data)/(max_data-min_data)