# © StrainMAPPER developer team 2023
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import tkinter as tk
from tkinter import filedialog
import os
import StrainMAPPER.EWPC_tools as et
from StrainMAPPER.signals import EWPCPeakArray, EWPCPeakSignal2D, EWPCSignal2D, LazyEWPCSignal2D
from hyperspy.io import load
import logging
import numpy as np
_logger = logging.getLogger(__name__)

def open_dialog(**kwargs):
    """
    Open a file dialog box to select a file.

    Parameters
    ----------
    **kwargs : optional
        Any additional keyword arguments to be passed to the filedialog.askopenfilename function.

    Returns
    -------
    file_path : str
        The absolute path of the selected file.
    file_dir : str
        The absolute path of the directory containing the selected file.
    file_name : str
        The name of the selected file.

    """  
    
    root = tk.Tk()
    root.attributes("-topmost", True)
    root.withdraw()
    file_path = filedialog.askopenfilename(parent=root, **kwargs)
    file_dir, file_name = os.path.split(file_path)
    return file_path, file_dir, file_name

def save_dialog(**kwargs):
    """
    Open a file dialog box to select a location to save a file.

    Parameters
    ----------
    **kwargs : optional
        Any additional keyword arguments to be passed to the filedialog.asksaveasfilename function.

    Returns
    -------
    save_path : str
        The absolute path of the location where the file will be saved.
    save_dir : str
        The absolute path of the directory where the file will be saved.
    save_name : str
        The name of the file to be saved.

    """
    
    root = tk.Tk()
    root.attributes("-topmost", True)
    root.withdraw()
    save_path = filedialog.asksaveasfilename(parent=root, **kwargs)
    save_dir, save_name = os.path.split(save_path)
    return save_path, save_dir, save_name

def variable_string(variables):
    """
    Construct a formatted string representing the variables used to generate the peak array.

    Parameters
    -----------
    variables : numpy.ndarray
        An array of variables used to generate the peak array, as saved by the save_peak_array function. The variables are stored in a structured array with the following fields:
        
            - 'n_clusters': int
            - 'r': int
            - 'r_outer': int
            - 'min_sigma': float
            - 'max_sigma': float
            - 'num_sigma': int
            - 'sigma_ratio': float
            - 'threshold': float
            - 'log_scale': bool

    Returns
    -------
    variable_string : str
        A formatted string representing the variables used to generate the peak array, with the following fields:
        
        |    | n_clusters  | {0} |
        |    | r_inner     | {1} |
        |    | r_outer     | {2} |
        |    | min_sigma   | {3} |
        |    | max_sigma   | {4} |
        |    | num_sigma   | {5} |
        |    | sigma_ratio | {8} |
        |    | threshold   | {6} |
        |    | log_scale   | {7} |
            
    """
    
    try:
        sigma_ratio = variables['sigma_ratio'][0]
    except:
        sigma_ratio = 'NA'
    variable_string = """
The following variables are now loaded:

| n_clusters  | {0} |
| r_inner     | {1} |
| r_outer     | {2} |
| min_sigma   | {3} |
| max_sigma   | {4} |
| num_sigma   | {5} |
| sigma_ratio | {8} |
| threshold   | {6} |
| log_scale   | {7} |
""".format(variables['n_clusters'][0], variables['r'][0],variables['r_outer'][0],
           variables['min_sigma'][0],variables['max_sigma'][0],variables['num_sigma'][0],
           variables['threshold'][0],variables['log_scale'][0], sigma_ratio)
    return variable_string

def open_peak_array(signal_shape=None):
    """
    Open a compressed numpy file containing peak arrays and return the peak array and
    associated variables.

    Parameters
    ----------
    signal_shape : tuple, optional
        The shape of the 4D dataset that the peak array was generated from. If not provided,
        the function will try to extract the shape from the `variables` array returned by the
        `load_peak_array` function. If the shape cannot be extracted, it will be assumed to be
        (128, 128, 128, 128).

    Returns
    -------
    peak_array : EWPCPeakArray
        An instance of the `EWPCPeakArray` class containing (x, y) coordinates of peak locations.
        The peak arrays are of varying sizes, and each peak array has shape (n, 2), where n is
        the number of peaks.

    variables : numpy.ndarray
        An array of variables used to generate the peak array, as saved by the
        `save_peak_array` function. The variables are stored in a structured array
        with the following fields:
        
            - 'n_clusters': int
            - 'r': int
            - 'r_outer': int
            - 'min_sigma': float
            - 'max_sigma': float
            - 'num_sigma': int
            - 'sigma_ratio': float
            - 'threshold': float
            - 'log_scale': bool

    """

    peak_file = open_dialog(filetypes = [('Numpy save', '.npz'), ('All files', '*')])
    peak_array, variables = et.load_peak_array(peak_file[0])
    if signal_shape is None:
        try:
            signal_shape = variables['signal_shape'][0]
        except:
            signal_shape = (128,128,128,128)
            _logger.warning('The shape of the 4D dataset could not be loaded and is not specified. It is assumed to be (128,128,128,128)')
    peak_array = EWPCPeakArray(peak_array, signal_shape)
    print(variable_string(variables))
    return peak_array, variables

def create_variable_array():
    """
    Create an empty NumPy structured array containing the default values (0) for the variables used in peak finding.

    Returns
    -------
    variables : ndarray
        A 1D array with a single element, where each field corresponds to a variable used in peak finding.
        The fields are:
            'n_clusters' : int
                The number of clusters to use in the KMeans clustering algorithm.
            'r' : int
                The inner radius of the circular mask used to only record EWPC peaks inside the mask.
            'r_outer' : int
                The radius of the outer boundary of the circular mask used to only record EWPC peaks inside the mask.
            'min_sigma' : float
                The smallest sigma to use in the Gaussian filter applied to the data.
            'max_sigma' : float
                The largest sigma to use in the Gaussian filter applied to the data.
            'num_sigma' : int
                The number of sigmas to use between min_sigma and max_sigma.
            'sigma_ratio' : float
                The ratio of consecutive sigmas in the Gaussian filter.
            'threshold' : float
                The threshold value used to mask the center of mass array.
            'log_scale' : bool
                If True, the sigmas will be logarithmically spaced between min_sigma and max_sigma,
                otherwise they will be linearly spaced.
    """
    
    variables = np.array([(0, 0, 0, 0, 0, 0, 0, 0, 0)],
                        dtype = [('n_clusters', int),
                                 ('r', int),
                                 ('r_outer', int),
                                 ('min_sigma',float),  
                                 ('max_sigma', float), 
                                 ('num_sigma', int),
                                 ('sigma_ratio', float), 
                                 ('threshold', float),
                                 ('log_scale', bool)])
    return variables


def peak_find_dict(peak_mode, variables):
    """
    Returns a dictionary of parameters for the specified peak finding mode, based on the given variables.

    Parameters
    ----------
    peak_mode : str
        The peak finding mode to use. Must be one of 'LoG', 'DoG', or 'DoH'.
    variables : numpy.ndarray
        An array of variables used to generate the peak array, as saved by the
        `save_peak_array` function. The variables are stored in a structured array
        with the following fields:
        
            - 'n_clusters': int
            - 'r': int
            - 'r_outer': int
            - 'min_sigma': float
            - 'max_sigma': float
            - 'num_sigma': int (only for 'LoG' and 'DoH' modes)
            - 'sigma_ratio': float (only for 'DoG' mode)
            - 'threshold': float
            - 'log_scale': bool (only for 'LoG' and 'DoH' modes)

    Returns
    -------
    peak_find_dict : dict
        A dictionary of parameters for the specified peak finding mode. The dictionary
        includes the following keys:
        
            - 'n_clusters': int
            - 'r': int
            - 'r_outer': int
            - 'min_sigma': float
            - 'max_sigma': float
            - 'threshold': float
            
        For the 'LoG' and 'DoH' modes, the dictionary also includes the following keys:
        
            - 'num_sigma': int
            - 'log_scale': bool
            
        For the 'DoG' mode, the dictionary also includes the following key:
        
            - 'sigma_ratio': float
            
    """
    
    peak_find_dict = dict(n_clusters = variables['n_clusters'][0], peak_mode = peak_mode, r = variables['r'][0],
                          r_outer = variables['r_outer'][0], min_sigma=variables['min_sigma'][0], max_sigma=variables['max_sigma'][0],
                          threshold = variables['threshold'][0])
    if peak_mode == 'LoG' or peak_mode == 'DoH':
        peak_find_dict.update(num_sigma = variables['num_sigma'][0], log_scale = variables['log_scale'][0])
    elif peak_mode == 'DoG':
        peak_find_dict.update(sigma_ratio = variables['sigma_ratio'][0])
    
    return peak_find_dict

def load_vector_map():
    """
    Load a vector map from a NumPy compressed (.npz) file.

    This function opens a file dialog to select a NumPy compressed file (.npz)
    containing data and associated metadata required to create an instance of
    the `EWPCPeakSignal2D` class. It then extracts the data, mask, and peak
    cluster information from the file, and constructs a `EWPCPeakSignal2D`
    object using these values.

    Returns
    -------
    EWPCPeakSignal2D
        An instance of the `EWPCPeakSignal2D` class initialized with the loaded data and metadata.

    Raises
    ------
    NoFileSelectedError
        If the user cancels the file selection dialog or selects no files.
    FileNotFoundError
        If the selected file does not exist.
    ValueError
        If the selected file is not a valid NumPy compressed file (.npz) or if it 
        lacks required data and metadata fields.
    """
    load_path = open_dialog(filetypes = [('numpy compressed', '*.npz')] ,defaultextension = '.npz')[0]
    with np.load(load_path) as npz:
        arr = np.ma.MaskedArray(npz['data'], npz['mask'])
        vector_map = EWPCPeakSignal2D(com= arr, peak_cluster1= int(npz['pc1']), peak_cluster2=int(npz['pc2']))
    return vector_map

def load_EWPC(lazy = False):
    """
    Load an Exit-Wave Power Cepstrum (EWPC) signal from a file.

    This function facilitates the loading of EWPC signals from files in the 'HSpy' format. 
    The function opens a file dialog to select the desired file, loads the data, and returns 
    an appropriate EWPC signal object. The choice between a lazy-loaded or fully-loaded EWPC 
    signal object is determined by the `lazy` parameter.

    Parameters:
    -----------
    lazy : bool, optional
        If True, load the EWPC signal lazily, meaning that the data is loaded only when accessed.
        If False (default), load the entire EWPC signal into memory.

    Returns:
    --------
    EWPC : EWPCSignal2D or LazyEWPCSignal2D
        An EWPC signal object representing the loaded data, depending on the `lazy` parameter.

    Notes:
    ------
    - The 'HSpy' file format is commonly used to store EWPC data.
    - Lazy loading can be useful for conserving memory when working with large EWPC datasets.
    """
    load_path = open_dialog(filetypes = [('HSpy', '.hspy')])[0]
    s = load(load_path, lazy=lazy)
    if lazy:
        EWPC = LazyEWPCSignal2D(s)
    else:
        EWPC = EWPCSignal2D(s)
        
    return EWPC