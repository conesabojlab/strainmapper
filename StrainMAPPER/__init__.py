# © StrainMAPPER developer team 2023
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



from StrainMAPPER import signals
from StrainMAPPER import EWPC_tools
from StrainMAPPER import io_tools
from StrainMAPPER.EWPC_tools import plot_single_EWPC
from StrainMAPPER.generate_ewpc import generate_ewpc
from StrainMAPPER.io_tools import open_dialog, open_peak_array, peak_find_dict, create_variable_array, load_vector_map
from StrainMAPPER.dummy_data import generate_dummy_data
from pixstem.io_tools import load_ps_signal

__all__ = [
    "load_ps_signal",
    "load_vector_map",
    "generate_ewpc",
    "open_ps_signal",
    "open_peak_array",
    "plot_single_EWPC",
    "peak_find_dict",
    "EWPC_tools",
    "signals",
    "io_tools",
    "generate_dummy_data"
]

def open_ps_signal(lazy=False, chunk_size=None, navigation_signal=None, **kwargs):
    """
    Load a Pixelated STEM (pSTEM) signal from a file using a file dialog box.

    Parameters
    ----------
    lazy : bool, optional
        If True, returns a lazy version of the signal. Default is False.
    chunk_size : int, optional
        If lazy is True, specifies the chunk size of the lazy signal. Default is None.
    navigation_signal : str, optional
        The name of the navigation signal in the file. If None, the signal will be 
        automatically determined based on the file format. Default is None.
    **kwargs : dict
        Additional arguments to be passed to the file dialog box.

    Returns
    -------
    PixelatedSTEMSignal (s)
        The loaded PixelatedSTEMSignal

    Raises
    ------
    FileNotFoundError
        If the file cannot be found or loaded.
    """
    filename = open_dialog(**kwargs)[0]
    s = load_ps_signal(filename, lazy = lazy, chunk_size = chunk_size, 
                        navigation_signal = navigation_signal)
    return s

