# © StrainMAPPER developer team 2023
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from StrainMAPPER import EWPC_tools as et
import numpy as np
import dask.array as da
from scipy import ndimage
from skimage.util import random_noise
from dask.diagnostics import ProgressBar
from StrainMAPPER.signals import LazyEWPCSignal2D, EWPCSignal2D
from pixstem.pixelated_stem_class import LazyPixelatedSTEM, PixelatedSTEM

def generate_single_EWPC(sig_shape, radius, rotation, seperation):
    """
    Parameters
    ----------
    sig_shape : tuple
        Shape (y, x, k_y, k_x) of the output EWPC array.
    radius : float
        Radius of the EWPC spots.
    rotation : float or None
        Rotation angle in degrees. If not None, the EWPC array will be rotated by the specified angle.
    seperation : float
        Seperation between the center of the EWPC and the EWPC peaks in pixels.

    Returns
    -------
    : numpy.ndarray
        A single EWPC array representing a dummy exit-wave power cepstrum.

    Note
    ----
    Rotation if not None and not 90, 180 or 270 will use interperloation to determine EWPC spot location.
    """
    
    EWPC = np.zeros(sig_shape)
    c = np.array((0,-seperation))
    for n in range(6):
        theta = np.radians(60*n)
        rot = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
        r = np.dot(rot,c)
        r = np.round(r)
        r[:] += ((sig_shape[0]/2, sig_shape[1]/2))
        mask = et._make_circular_mask(r[0],r[1],sig_shape[1], sig_shape[0],radius)
        EWPC += mask
    if rotation is not None:
        EWPC = ndimage.rotate(EWPC, angle = rotation, reshape=False, order=5)
        
    EWPC += et._make_circular_mask(sig_shape[1]/2,sig_shape[0]/2,sig_shape[1], sig_shape[0],radius)
    return da.array(EWPC)

def make_4D_EWPC(EWPC, nav_shape):
    """
    Create a 4D EWPC array

    Parameters
    ----------
    EWPC : numpy.ndarray
        2D array representing the EWPC.
    nav_shape : tuple
        Shape of the navigation dimensions for repetition.

    Returns
    -------
    : numpy.ndarray
        A 4D array containing repeated copies of the EWPC map.

    Example
    -------
    >>> EWPC = generate_single_EWPC((256, 256), 10, 45)
    >>> nav_shape = (5, 5)
    >>> EWPC_4D = make_4D_EWPC(EWPC, nav_shape)
    >>> # EWPC_4D.shape will be (5, 5, 256, 256)
    
    """

    EWPC_4D = da.repeat(EWPC[np.newaxis, :, :], nav_shape[1], axis=0)
    EWPC_4D = da.repeat(EWPC_4D[np.newaxis,:, :, :], nav_shape[0], axis=0)
    return EWPC_4D

def add_noise(data, intensity, mode, gaussian_filter, g_kwds, **noise_kwds):
    """
    Add noise to a given data array based on the specified parameters.

    Parameters
    ----------
    data : numpy.ndarray
        Input 4D data array.
    intensity : float
        Intensity of the signal
    mode : str
        Noise mode identifier (e.g., 'uniform', 'gaussian').
    gaussian_filter : bool
        Flag indicating whether to apply Gaussian filtering.
        
           - If False: Multiple shifted versions of the original dataset will be averaged.
           - A random (subpixel)shift with values taken from np.random.normal(loc = 0, scale = 1, size=(max_i,2))
           
    g_kwds : dict
        Keyword arguments for Gaussian filtering.
    **noise_kwds : dict
        Additional keyword arguments specific to the noise mode.

    Returns
    -------
    : dask.ndarray
        Noisy data array with added noise.


    Example
    -------
    >>> data = np.random.rand(256, 256)
    >>> noisy_data = add_noise(data, intensity = 0.1, mode = 'gaussian', gaussian_filter = True, g_kwds = {'sigma': 2.0}, mean = 0.0, var = 0.01)
        
    """
    
    nav_dim = data.shape[:2]
    noisy_data = np.zeros_like(data)
    for yy,xx in np.ndindex(nav_dim):
        pixel = data[yy,xx]
        noisy_data[yy,xx] = intensity*add_noise_chunck(pixel, mode, 
                                                       gaussian_filter, g_kwds, **noise_kwds)
    return noisy_data

def add_noise_chunck(pixel, mode, gaussian_filter, g_kwds, steps = 5, **noise_kwds):
    """
    Add noise to a given data array based on the specified parameters.

    Parameters
    ----------
    data : numpy.ndarray
        Input 2D data array.
    intensity : float
        Intensity of the signal
    mode : str
        Noise mode identifier (e.g., 'uniform', 'gaussian').
    gaussian_filter : bool
        Flag indicating whether to apply Gaussian filtering.
        
           - If False: Multiple shifted versions of the original dataset will be averaged.
           - A random (subpixel)shift with values taken from np.random.normal(loc = 0, scale = 1, size=(max_i,2))
           
    g_kwds : dict
        Keyword arguments for Gaussian filtering.
    steps : int
        Number of random shifts to apply for noise generation.
    **noise_kwds : dict
        Additional keyword arguments specific to the noise mode.
    """
    
    if gaussian_filter:
        pixel = ndimage.gaussian_filter(pixel, **g_kwds)
    else:
        pixel_shift = np.zeros_like(pixel)
        max_i = steps
        random_shift = np.random.normal(loc = 0, scale = 1, size=(max_i,2))
        for i in random_shift:
            pixel_shift += ndimage.shift(pixel, shift=tuple(i), mode = 'nearest', order = 5)
        pixel = pixel_shift/max_i
        
    pixel = random_noise(pixel, mode=mode, **noise_kwds)
    return pixel

def generate_dummy_data(shape, dummy_type = 'EWPC', radius = 2, rotation = None, seperation = 15, noise = False, 
                        intensity = 300, clip = True, mode = 'gaussian', gaussian_filter = False, 
                        g_kwds = {'sigma': 0.5},lazy_result=False, show_progressbar=False,
                        chunk_calculations=None, **noise_kwds):
    
    """
    Generate a dummy Exit-Wave Power Cepstrum (EWPC) or Convergent Beam Electron Diffraction (CBED) signal based on the specified parameters.

    Parameters
    ----------
    shape : tuple
        Shape of the output signal in the format (x, y, k_x, k_y).
    dummy_type : str
        Specify the type of the Dummy Signal. Can either be 'EWPC' or 'CBED' (default = EWPC).
    radius : int
        Radius of EWPC spots (default: 2).
    rotation : float or None
        Angle of rotation in degrees for EWPC generation (default: None).
    seperation : float
        Seperation between the center of the EWPC and the EWPC peaks in pixels (default = 15). 
    noise : bool
        Flag indicating whether to add noise to the generated signal (default: False).
    intensity : float
        Intensity of the base signal (default: 300).
    mode : str
        Noise mode identifier for noise addition (default: 'gaussian').
    gaussian_filter : bool
        Flag indicating whether to apply Gaussian filtering during noise addition (default: False).
        
           - If False: Multiple shifted versions of the original dataset will be averaged.
           - A random (subpixel)shift with values taken from np.random.normal(loc = 0, scale = 1, size=(max_i,2))
           
    g_kwds : dict
        Keyword arguments for Gaussian filtering (default: {'sigma': 0.5}).
    lazy_result : bool
        Flag indicating whether to return a lazy computation result (default: False).
    show_progressbar : bool
        Flag indicating whether to display a progress bar during computation (default: False).
    chunk_calculations : list or None
        Chunk sizes for computation (default: None).
    **noise_kwds : dict
        Additional keyword arguments specific to the noise mode.

    Returns
    -------
    For dummy_type = 'EWPC'
        EWPCSignal2D or LazyEWPCSignal2D
            The generated 4D EWPC signal as an instance of EWPCSignal2D or LazyEWPCSignal2D, depending on the value of lazy_result.

    For dummy_type = 'CBED'
        LazyPixelatedSTEM or PixelatedSTEM
            The generated 4D EWPC signal as an instance of PixelatedSTEM or LazyPixelatedSTEM, depending on the value of lazy_result.

    Example
    -------
    >>> shape = (64, 64, 128, 128)
    >>> radius = 2
    >>> rotation = 45
    >>> noise = True
    >>> intensity = 100
    >>> mode = 'gaussian'
    >>> gaussian_filter = True
    >>> g_kwds = {'sigma': 1.0}
    >>> lazy_result = False
    >>> show_progressbar = True
    >>> chunk_calculations = [16, 16, 128, 128]
    >>> noise_kwds = {'sigma': 0.1}
    >>> EWPC = generate_dummy_data(shape, radius, rotation, noise, intensity, mode, gaussian_filter, g_kwds,lazy_result, show_progressbar, chunk_calculations, **noise_kwds)

    See Also:
        - generate_single_EWPC: Function for generating a single EWPC map.
        - make_4D_EWPC: Function for creating a 4D array of EWPC maps.

    """
    
    if dummy_type not in ['EWPC', 'CBED', 'ewpc', 'cbed']:
        raise TypeError ("dummy_type must be 'EWPC' or 'CBED' not [{0}]".format(dummy_type))
    
    nav_shape = (shape[1], shape[0])
    sig_shape = (shape[3], shape[2])

    if chunk_calculations is None:
        chunk_calculations = [16] * len(nav_shape) + list(sig_shape)

    data = generate_single_EWPC(sig_shape, radius, rotation, seperation)
    
    if dummy_type == 'CBED':
        data = et.iEWPC(data)
        data = et.normalize_array(data)*intensity
        clip = False
        intensity = 1
        
    data = make_4D_EWPC(data, nav_shape).rechunk(chunks = chunk_calculations)
    if noise:
        data = da.map_blocks(add_noise, data, intensity, mode, gaussian_filter, g_kwds, clip = clip, **noise_kwds)
    else:
        data = data * intensity
    
    if lazy_result:
        if dummy_type == 'EWPC':
            out = LazyEWPCSignal2D(data)
        else:
            out = LazyPixelatedSTEM(data)
    else:
        if show_progressbar:
            pbar = ProgressBar()
            pbar.register()
        data = data.compute()
        if show_progressbar:
            pbar.unregister()
            
        if dummy_type == 'EWPC':
            out = EWPCSignal2D(data)
        else:
            out = PixelatedSTEM(data)
    return out