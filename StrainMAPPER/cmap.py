from matplotlib.colors import LinearSegmentedColormap

def custom_cmap():
    a=256
    colors = [(20/a, 29/a, 67/a), (28/a,77/a,96/a), (16/a,125/a,121/a), (93/a,167/a,134/a), (181/a,202/a,175/a), (236/a,225/a,210/a), 
                (230/a,183/a,161/a), (211/a,119/a,105/a), (173/a,63/a,95/a), (117/a,25/a,93/a), (14/a,14/a,14/a)] # vorticity adjusted
    n_bin = 1000  # Discretizes the interpolation into bins
    cmap_name = 'vorticity-custom'
    cmap = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bin)
    return cmap
