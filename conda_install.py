import os
import sys
import shutil
import glob
import traceback

try:
    api_file_list = glob.glob('StrainMAPPER\**', recursive=True)
    p_path = os.path.split(sys.executable)[0]
    p_path = os.path.join(p_path, 'Lib\site-packages')
    for api_file in api_file_list:
        dest_dir = os.path.join(p_path, api_file)
        if not os.path.isfile(api_file):
            os.makedirs(dest_dir, exist_ok=True)
        else:
            shutil.copyfile(os.path.join(os.getcwd(), api_file), dest_dir)
    print('StrainMAPPER files succesfully copied to:\n%s'%(p_path))
except Exception as E:
    print('unable to copy necesarry StrainMAPPER files to:\n%s\n\n\n%s'%(p_path, E))