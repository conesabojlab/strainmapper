# %% [markdown]
# ## Loading packages and data
# - This code imports the `StrainMAPPER` package and loads the `open_ps_signal()` function from it. The `lazy=True` argument specifies that the data should be loaded lazily, meaning that it will not be loaded into memory until it is needed.
# - The `filetypes` argument specifies the file types that can be opened by the function, with the options of selecting only `XML` files or all files.
# - The `%matplotlib qt` activates interactive plotting with `matplotlib`, which allows the user to zoom and pan on the displayed plots.

# %%
# %matplotlib qt
import StrainMAPPER

# %%
STEM_4D = StrainMAPPER.open_ps_signal(lazy=True, filetypes = [('XML file', '.xml'), ('All files', '*')])

# %% [markdown]
# ## Generating and plotting the EWPC
# - The first block generates and plots the EWPC (Exit-Wave Power Cepstrum)
# - The second block of code generates an annular dark field (ADF) image of the electron microscopy data using the `virtual_annular_dark_field()` function with a specified inner and outer radius. The `show_progressbar` argument is set to True, which displays a progress bar while the ADF image is being generated.
# - Next, generate the EWPC plot using the `plot()` function. The navigator argument is set to `s_adf`, which is the previously generated ADF image, and norm is set to log, which normalizes the data using a logarithmic scale. This produces a visualization of the EWPC for each pixel in the ADF image.

# %%
EWPC = StrainMAPPER.generate_ewpc(STEM_4D, lazy_result=True)


# %%
s_adf = STEM_4D.virtual_annular_dark_field(64,64,r_inner=45, r= 64, show_progressbar=True)
EWPC.plot(navigator=s_adf, norm = 'log')

# %% [markdown]
# ## [Alternative] Generating a dummy EWPC dataset
# Instead of using the above code to load an actual dataset, a dummy dataset can also be generated. See the `docstring` for an explanation of all the variables

# %%
EWPC = StrainMAPPER.generate_dummy_data(shape = (128,128,128,128), dummy_type = 'EWPC', radius = 2, rotation=30, seperation = 48, 
                                        noise = True, intensity= 350, mean = 1E-1, gaussian_filter=False, g_kwds=dict(sigma = 0.5),
                                        lazy_result=False, show_progressbar=True)

# %%
EWPC.plot()

# %% [markdown]
# #### Save or Load EWPC dataset
# Options to save and/or load EWPC (dummy) datasets for later use.

# %%
save_path = StrainMAPPER.io_tools.save_dialog(filetypes = [('HSpy', '.hspy')], defaultextension = '.hspy')[0]
EWPC.save(save_path)

# %%
EWPC = StrainMAPPER.io_tools.load_EWPC()

# %% [markdown]
# ## Tracking the EWPC peaks in the 4D dataset

# %% [markdown]
# ### Select a single EWPC
# Select a single EWPC to preform initial analysis on

# %%
EWPC_pixel = EWPC.data[50,50,:,:]

# %% [markdown]
# ### Specify the variables for the Peak Track
# This code creates a Python dictionary called variables that sets several variables to be used in the StrainMAPPER algorithm. Here's what each variable means:
# - `n_clusters` : the number of clusters to group the data into.
# - `r`: The inner radius (in pixels) of the circular mask used to only record EWPC peaks inside the mask.
# - `r_outer` : the outer radius (in pixels) the circular mask used to only record EWPC peaks inside the mask.
# - `min_sigma` : the minimum standard deviation of the Gaussian filters used to filter the data.
# - `max_sigma` : the maximum standard deviation of the Gaussian filters used to filter the data.
# - `num_sigma` : the number of standard deviations to use between min_sigma and max_sigma.
# - `sigma_ratio` : the ratio between the standard deviations of consecutive Gaussian filters.
# - `threshold` : the threshold for selecting peaks in the filtered data.
# - `log_scale` : whether to use a log scale for the intensity of the peaks in the filtered data.

# %%
variables = StrainMAPPER.create_variable_array()
variables['n_clusters']= 6
variables['r']= 42
variables['r_outer']= 54
variables['min_sigma']= 2.0
variables['max_sigma']= 2.5
variables['num_sigma']= 10
variables['sigma_ratio']= 1.8
variables['threshold']= 20
variables['log_scale']= False

# %% [markdown]
# ### Find EWPC peaks in a single frame
# - Use the variables above to find peaks in a single EWPC frame
# - Different blob detection algorithms can be used (default = `DoG`)
# - See [`skimage` Blob Detection](https://scikit-image.org/docs/stable/auto_examples/features_detection/plot_blob.html) for more information
#

# %%
fig = StrainMAPPER.plot_single_EWPC(EWPC_pixel,
                                    **StrainMAPPER.peak_find_dict('DoG', variables),
                                    plot_mask=True,
                                    image_kwds = dict(norm = None, vmax = 200))

# %% [markdown]
# ### Find EWPC peaks in all the frames
# Use the same variables to find EWPC peaks in all the frames of the 4D dataset

# %%
peak_array = EWPC.peak_find(lazy_result=False, show_progressbar=True, 
                            **StrainMAPPER.peak_find_dict('DoG', variables))

# %%

# %% [markdown]
# #### Save peak_array
# Using this function you can save the peak_array for later use. It will also save the values of the variables array

# %%
save_path = StrainMAPPER.io_tools.save_dialog(filetypes = [('NPZ', '.npz')], defaultextension = '.npz')[0]
StrainMAPPER.EWPC_tools.save_peak_array(save_path, peak_array.data, *variables[0])

# %% [markdown]
# ### [Alternative] Load an external peak_array save file
#

# %%
peak_array, variables = StrainMAPPER.open_peak_array(signal_shape=(128,128,128,128))

# %% [markdown]
# ## Cluster the EWPC peaks 
# The code is clustering the peaks in the peak array into n clusters using the `K-means` clustering algorithm. Here's what each argument does:
# - `n_clusters` : specifies the number of clusters to group the peaks into.
# - `threshold` : sets a minimum threshold value for the peak intensities/counts.
# - `background` : sets the image to be used as a background in the scatter plot.
# - see [`sklearn` Kmeans](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html) for more information on `K-means` clustering
#

# %%
cluster = peak_array.cluster_peaks(n_clusters=6, threshold=0, n_init = 100, 
                                   random_state = 0, background = EWPC_pixel,
                                   background_kwds = dict(norm = None, vmax = 200),
                                   scatter_kwds = dict(cmap = 'Spectral_r'),
                                   image_kwds = dict(cmap = 'viridis', alpha=0.3))

# %% [markdown]
# ### Apply a threshold to the peak_array
# - The `threshold` parameter specifies the minimum count required to keep a peak. Peaks whose count is
# less than the threshold will be removed. The `threshold_max` parameter specifies the maximum count
# to keep a peak. Peaks whose count exceeds this value will also be removed.
# - If `inplace` is `True`, the peak array is modified in-place and None is returned. If `inplace` is False,
# a new `EWPCPeakArray` object is returned containing the trimmed peak array.

# %%
peak_array.threshold(threshold = 25, inplace=True)

# %% [markdown]
# ## Find the subpixel maximum for two clusters 
# Generate a vector map by identifying and measuring the sub-pixel position of peaks in the Exit-Wave Power Cepstrum (EWPC) based on the peak locations of EWPC peaks in two clusters.
# Here is an explanation of the parameters used in the function call:
# - `cluster` : a PeakCluster object that represents the cluster information of the peaks.
# - `peak_cluster1` (x') : an integer that specifies the cluster label of the first peak for the vector calculation.
# - `peak_cluster2` (y') : an integer that specifies the cluster label of the second peak for the vector calculation.
# - `mask_r` : an integer that sets the radius of the mask around each peak used for the vector calculation.
# - `threshold` : When the value for the sub-pixel position doesn't meet the threshold, the value is masked.
# - `center` : a Boolean that specifies whether to use pixel (0,0) (if False) or the center of the EWPC pattern (if True) as origin for the vector coordinates. For the strain calculations it is important to set this flag to True.

# %%
vector_map = EWPC.subpixel_peak_find(peak_array, cluster, peak_cluster1 = 3, peak_cluster2 = 2,
                            mask_r = 3, threshold = 3, center = True, lazy_result = False, show_progressbar = True)

# %% [markdown]
# #### Save or Load a vector_map for later use
# The saved file will include the data, mask, and peak cluster information, making it possible to later load and reconstruct the vector map.

# %%
vector_map.save()

# %%
vector_map = StrainMAPPER.load_vector_map()

# %% [markdown]
# ### Plot subpixel locations
# Plot the data in a 2D grid of images, where each image corresponds to a cluster
# of peaks. Alternatively, plot a single cluster image.
# - see `docstring` for more information

# %%
fig = vector_map.plot(single = False, index = 0)

# %% [markdown]
# ## Generate and plot the strain matrices
# This code generates and plots the strain matrices using the vector maps.
#
# - A reference position is chosen by specifying a range of pixels in the `vector_map.reference([x1,x2],[y1,y2])` function.
#
#
# - Next, the `vector_map.strain_matrix()` calculates the strain between each point in the map and the reference point. 
#     - `refCoords` : if True, the output strain matrices will be computed in the reference coordinate system (default is `True`).
#     - `car_basis` : if True, the deformation gradient will be computed with respect to the cartesian basis vectors (default is `False`).
#     - `export` : if True, the function returns the strain and rotation matrices instead of updating the object's attributes (default is `False`).
#
#
# - Finally, the `vector_map.plot_strain()` function is called to visualize the strain matrix. 
#     - The `gaussian_blur` parameter controls the amount of smoothing applied to the image, 
#     - The `single` and `index` parameters control which strain matrix is plotted (see `docstring`).
#     - The `equal_scale` parameter ensures that the strain values are plotted on the same scale across all panels.

# %%
reference = vector_map.reference([5,10], [5,10])

# %%
vector_map.strain_matrix(reference, refCoords=True, car_basis=False, export=False)

# %%
fig = vector_map.plot_strain(gaussian_blur=0.5, single=False, index=0, equal_scale=False)
