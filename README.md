 # StrainMAPPER
**StrainMAPPER** is a Python package that calculates local strain variations within a specimen by comparing the measured atomic spacing across the specimen with respect to a reference area. For more information, including a tutorial and citation policy, see the [documentation](https://sconesaboj.gitlab.io/strainmapper-documentation/).

## Notes
**StrainMAPPER** has been developed for and tested with `.XML` and `.raw` files of the Electron Microscope Pixel Array Detector (EMPAD) from [**thermo**scientific](https://assets.thermofisher.com/TFS-Assets/MSD/Datasheets/EMPAD-Datasheet.pdf). However, the methods in **StrainMAPPER** are fully universal and should be compatible with any 4D-dataset supported by [hyperspy](https://hyperspy.org/hyperspy-doc/current/user_guide/io.html) and [pixStem](https://pixstem.org/loading_data.html).  

## License
© StrainMAPPER developer team 2023

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
